package com.dvereykin.program6;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static android.content.ContentValues.TAG;

/**
 * Created by Dmitry Vereykin aka eXrump on 9/29/2016.
 */


public class EditValuesActivity extends Activity implements View.OnClickListener {
    Button saveButton;
    Button clearButton;
    Button cancelButton;
    Button photoButton;

    EditText editName;
    EditText editAddress;
    EditText editCity;
    EditText editState;
    EditText editZipCode;

    ImageView imagePhoto;

    int result;
    int CAMERA_PIC_REQUEST = 2;
    PersonalDataIntent pdIntent;
    String imagePath = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_values);

        imagePhoto = (ImageView) findViewById(R.id.imagePhoto);

        saveButton = (Button) findViewById(R.id.save_button);
        clearButton = (Button) findViewById(R.id.clear_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        photoButton = (Button) findViewById(R.id.photo_button);

        editName = (EditText) findViewById(R.id.editNameFld);
        editAddress = (EditText) findViewById(R.id.editAddressFld);
        editCity = (EditText) findViewById(R.id.editCityFld);
        editState = (EditText) findViewById(R.id.editStateFld);
        editZipCode = (EditText) findViewById(R.id.editZipCodeFld);

        saveButton.setOnClickListener(this);
        clearButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        photoButton.setOnClickListener(this);

        pdIntent = new PersonalDataIntent(getIntent());
        imagePath = pdIntent.image;

        if (isStoragePermissionGranted())
            loadImage();
        else
            showInfo("Permission is not granted!");

        editName.setText(pdIntent.name);
        editAddress.setText(pdIntent.address);
        editCity.setText(pdIntent.city);
        editState.setText(pdIntent.state);
        editZipCode.setText(pdIntent.zipCode);

        if (pdIntent.action == PersonalDataIntent.ActionType.DELETE) {
            saveButton.setText(R.string.delete_button_str);
            editName.setEnabled(false);
            editAddress.setEnabled(false);
            editCity.setEnabled(false);
            editState.setEnabled(false);
            editZipCode.setEnabled(false);
            clearButton.setEnabled(false);
            photoButton.setEnabled(false);
        }

        if (pdIntent.action == PersonalDataIntent.ActionType.VIEW) {
            saveButton.setText(R.string.ok_button_str);
            editName.setEnabled(false);
            editAddress.setEnabled(false);
            editCity.setEnabled(false);
            editState.setEnabled(false);
            editZipCode.setEnabled(false);
            clearButton.setEnabled(false);
            photoButton.setEnabled(false);
        }

        if (pdIntent.action == PersonalDataIntent.ActionType.EDIT) {
            saveButton.setText(R.string.update_button_str);
        }

    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            loadImage();
        }
    }

    void loadImage() {
        if (imagePath == null || imagePath.length() == 0)
            return;

        //Bitmap imageBitmap;
        //BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 3;
        //imageBitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/"+ imagePath, options);
        //imagePhoto.setImageBitmap(imageBitmap);
        //System.out.println(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/"+ imagePath);

        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        imagePhoto.setImageBitmap(BitmapFactory.decodeFile(storageDir + "/DCIM/" + imagePath));
    }

    public void clear() {
        editName.setText("");
        editAddress.setText("");
        editState.setText("");
        editCity.setText("");
        editZipCode.setText("");
    }

    @Override
    public void finish() {

        pdIntent.clearIntent();
        pdIntent.name = editName.getText().toString();
        pdIntent.address = editAddress.getText().toString();
        pdIntent.city = editCity.getText().toString();
        pdIntent.state = editState.getText().toString();
        pdIntent.zipCode = editZipCode.getText().toString();
        pdIntent.image = imagePath;

        setResult(result, pdIntent.getIntent());
        super.finish();
    }

    public void onClick(View v) {
        if (saveButton.getId() == v.getId()) {
            result = RESULT_OK;
            finish();

        }

        if (clearButton.getId() == v.getId()) {
            clear();
        }

        if (cancelButton.getId() == v.getId()) {
            result = RESULT_CANCELED;
            finish();

        }

        if (photoButton.getId() == v.getId()) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imagePath = "Address" + System.currentTimeMillis() + ".png";
            Uri uriSavedImage = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/DCIM", imagePath));
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
        }
    }

    void showInfo(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    loadImage();
                } catch (Exception e) {
                    showInfo("Picture not taken");
                    e.printStackTrace();
                    imagePath = "";
                }
            }
        } else {
            showInfo("Picture not taken");
        }

    }
}



